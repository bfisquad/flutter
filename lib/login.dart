import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projekt_szklarnia/widgets.dart';
import 'package:responsive_scaffold/responsive_scaffold.dart';
import 'package:projekt_szklarnia/main.dart';
import 'package:firebase_auth/firebase_auth.dart';


class SignUpApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Czeska Sauna',
      routes: {
        '/_' : (context) => MyApp(),
        '/': (context) => SignUpScreen(),
      },
    );
  }
}
//TODO usunąć przed wypuszczeniem apki
void backDoor(context){
  Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
}
class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end:Alignment.bottomCenter,
                  colors: [Colors.lightGreen[50], Colors.white70,Colors.white],
                  stops: [0.1, 0.4, 0.7]
              )),
              child: SizedBox(
                width: 600,
                height: 300,
                child: SignUpForm(),
              ),
            )
        )
    );
  }
}

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _loginTextController = TextEditingController();
  final _passwordTextController = TextEditingController();
  String _login, _password;

  double _formProgress = 0;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void _goToMainPage() {
    Navigator.of(context).pushNamed('/_');
  }

  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Panel logowania',
            style: TextStyle(color: Colors.lightGreen[300],
                fontFamily: 'Arial',
                fontSize: 30.0,
                fontWeight: FontWeight.normal),
          ),

          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10.0),
              Container(
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.lightGreen[300])),
                height: 60.0,
                child: TextFormField(
                  controller: _loginTextController,
                  onSaved: (_loginTextController) => _login = _loginTextController,
                  decoration: InputDecoration(hintText: 'Email',
                      prefixIcon: Icon(
                          Icons.email, color: Colors.lightGreen[300])),
                ),
                padding: EdgeInsets.all(8.0),
              ),
              //Padding(
              //padding: EdgeInsets.all(8.0),
              //child: TextFormField(
              //controller: _loginTextController,
              //decoration: InputDecoration(hintText: 'Login'),
              //),
            ],
          ),

          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10.0),
              Container(
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.lightGreen[300])
                ),
                height: 60.0,
                child: TextFormField(
                  controller: _passwordTextController,
                  obscureText: true,
                  onSaved: (_passwordTextController) => _password = _passwordTextController,
                  decoration: InputDecoration(hintText: 'Hasło',
                      prefixIcon: Icon(
                          Icons.lock, color: Colors.lightGreen[300])),
                ),
                padding: EdgeInsets.all(8.0),
              ),
            ],
          ),

          Container(
              padding: EdgeInsets.symmetric(vertical: 15.0),
              child: Row(
                children: [
                  RaisedButton(
                      elevation: 5.0,
                      onPressed: signIn,
                      padding: EdgeInsets.all(15.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)
                      ),
                      child: Text('  Zaloguj się  '),
                      color: Colors.lightGreen[300]

                  ),
                  SizedBox(width: 30,),
                  RaisedButton(
                      elevation: 5.0,
                      onPressed: ()=> backDoor(context),
                      padding: EdgeInsets.all(15.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)
                      ),
                      child: Text('Mamy zabezpieczenia jak Apple, wbijaj bez logowania'),
                      color: Colors.lightGreen[300]

                  ),
                ],
              )
          )
        ],
      ),
    );
  }
  void _AlertWindow_() {
    showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text("Niepoprawne dane logowania"),
          content: Text("Wpisz ponownie dane logowania"),
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              child: Text("Ok", style: TextStyle(color: Colors.lightGreen[300])),
              onPressed: () { print("Ok");
              Navigator.of(context).pop();
              },
            )
          ],
        )
    );
  }

  Future<void> signIn() async {
    final _form = _formKey.currentState;
    if (_form.validate()) {
      _form.save();
      try {
        UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: _login,
            password: _password
        );
        Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
      }
      on FirebaseAuthException catch (error) {_AlertWindow_();}
      catch (error) {_AlertWindow_();}
    }
  }
}