import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projekt_szklarnia/main.dart';

//TODO : Tutaj proszę dawać bloki z wykresami

// przykład karty zawierającej wykres oraz kontrolki itp
class ExampleCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.grey,
      margin: EdgeInsets.all(15.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        //tutaj dodaj interfejs karty, wykresy i inne gówno
      ),
    );
  }
}

class TempCard extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: 10.0,
      margin: EdgeInsets.all(15.0),
      shape:RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(

              children: [
                Text("Hello there"),
                Expanded(
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children:[
                      Text("General Kenobi")

                    ]
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Chart(),
              SizedBox(width: 40,)
            ],
          ),
        ],
      ),
    );
  }

}

class Chart extends StatelessWidget {
  final textstyle = TextStyle(color: Colors.white, fontSize: 11);

  @override
  Widget build(BuildContext context) {
    return Flexible(
        child: SizedBox(
          height: 300,
          child: AspectRatio(
            aspectRatio: 1.5,
            child: LineChart(LineChartData(
                titlesData: FlTitlesData(
                  rightTitles: SideTitles(
                    showTitles: false,
                  )
                ),
                borderData: FlBorderData(
                    border: Border(
                      bottom: BorderSide(),
                      left: BorderSide(),
                    )
                ),
                gridData: FlGridData(show: false,),
                lineTouchData: LineTouchData(enabled: false),
                minX: 0,
                maxY: 20,
                minY: 0,
                lineBarsData: [
                  LineChartBarData(
                      spots: fakeData,
                      dotData: FlDotData(
                        show: false,
                      ),
                      isCurved: true,
                      belowBarData: BarAreaData(
                        show: true,
                        colors: [Colors.red],
                      )
                  )

                ]
            )
            ),

          ),
        )
    );
  }
}
final fakeData=[
  FlSpot(0, 10),
  FlSpot(1, 13),
  FlSpot(3, 7),
  FlSpot(4, 10),
  FlSpot(5, 10),
  FlSpot(6, 15),
  FlSpot(7, 10),
  FlSpot(8, 13),
  FlSpot(9, 7),
  FlSpot(10, 10),
  FlSpot(11, 10),
  FlSpot(12, 15),
];

