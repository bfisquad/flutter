import 'package:flutter/material.dart';
import 'package:projekt_szklarnia/database.dart';
import 'chart_widgets.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'database.dart';

// Ja pierdolę ale tu syf, ciekawe kto tak zrobił...
int columns = 1;

class Grid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context,constraints){
        if(constraints.maxWidth<700){
          columns=1;
        }else{
          columns=2;
        }
        return ResponsiveColumnGrid();
    },
    );
  }
}
class ResponsiveColumnGrid extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return GridView.count(
        crossAxisCount: columns,
        //childAspectRatio: 816/483,
        padding: EdgeInsets.all(10.0),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        children: [
          TempCard(),
          ExampleCard(),
          ExampleCard(),
          ExampleCard(),
          ExampleCard(),
          FlatButton(
            onPressed: addData(),
            child: Text('firestore test'),
          ),
          // Image.network("https://calculatic.pl/poradnik/wp-content/uploads/2019/12/2-wykres-skumulowany.png"),
          // Image.network("https://calculatic.pl/poradnik/wp-content/uploads/2019/12/2-wykres-skumulowany.png"),
          // Image.network("https://calculatic.pl/poradnik/wp-content/uploads/2019/12/2-wykres-skumulowany.png"),
          // Image.network("https://calculatic.pl/poradnik/wp-content/uploads/2019/12/2-wykres-skumulowany.png"),
          // Image.network("https://calculatic.pl/poradnik/wp-content/uploads/2019/12/2-wykres-skumulowany.png"),
          // Image.network("https://calculatic.pl/poradnik/wp-content/uploads/2019/12/2-wykres-skumulowany.png"),
          // Image.network("https://calculatic.pl/poradnik/wp-content/uploads/2019/12/2-wykres-skumulowany.png"),
          // Image.network("https://calculatic.pl/poradnik/wp-content/uploads/2019/12/2-wykres-skumulowany.png"),
        ],
    );
  }

}


class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 100,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              'Menu',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
            decoration: BoxDecoration(
                color: Colors.lightGreen,
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage("https://www.daglezja.pl/zdjecia/edytor/images/banner%20centrum.jpg"))),
          ),
          ListTile(
            leading: Icon(Icons.input),
            title: Text('Welcome'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Profile'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Settings'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.border_color),
            title: Text('Feedback'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}

